const allLogo = {
  background: require('./img/background.png'),
  logo: require('./img/logo.png'),
  icVisibilityOn: require('./img/ic_visibility_on.png'),
  icVisibilityOff: require('./img/ic_visibility_off.png'),
  icGoogle: require('./img/ic_google.png'),
  icFacebook: require('./img/ic_facebook.png'),
  loading: require('./img/loading.gif'),
  backgroundHeader: require('./img/background_header.png'),
  icHome: require('./img/ic_home.png'),
  icUser: require('./img/ic_user.png'),
  icSetting: require('./img/ic_setting.png'),
  icArrow: require('./img/ic_arrow.png'),
  imgUnderstand: require('./img/img_understand.png'),
  imgExam: require('./img/img_exam.png'),
  imgRanking: require('./img/img_ranking.png'),
  icMateri: require('./img/ic_materi.png'),
  icSilang: require('./img/ic_silang.png'),


}

export { allLogo }
