import firebase from 'firebase/app'
// Add the Firebase services that you want to use
import 'firebase/auth'
import 'firebase/database'
import 'firebase/firestore'
import 'firebase/storage'

// TODO: Replace the following with your app's Firebase project configuration
const firebaseConfig = {
  apiKey: "AIzaSyC88iSZcUashXBCvjPkVIw0Kz578jvkTEY",
  authDomain: "baqi-f0749.firebaseapp.com",
  projectId: "baqi-f0749",
  storageBucket: "baqi-f0749.appspot.com",
  messagingSenderId: "660843010578",
  appId: "1:660843010578:web:52463e4c8f1d461a8fd6cb",
  measurementId: "G-4DGY2J8FB7"
};

const firebaseApp = firebase.initializeApp(firebaseConfig);

var auth = firebaseApp.auth()
var database = firebaseApp.database()
var firestore = firebaseApp.firestore()
var storage = firebaseApp.storage()

export {
  auth,
  database,
  firestore,
  storage
}
