import React, { useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  StatusBar,
  ImageBackground,
  Pressable
} from "react-native";
import { allLogo } from '@Assets';
import { toDp } from '@percentageToDP';
import NavigatorService from '@NavigatorService'

const Home = (props) => {

  const presableMenu = (backgroundColor, image, title, desc, onPress) => {
    return (
      <Pressable style={[styles.presableMenu, {backgroundColor}]} onPress={() => onPress()}>
        <Image source={image} style={styles.imgUnderstand} />
        <View style={styles.viewText}>
          <Text style={styles.title}>{title}</Text>
          <Text style={styles.desc}>{desc}</Text>
        </View>
      </Pressable>
    )
  }

  return (
    <View style={styles.container}>
      {
        presableMenu(
          '#CBDFBD',
          allLogo.imgUnderstand,
          'Understand Qur’an',
          'Understand The Qur’an Well',
          () => NavigatorService.navigate('UnderstandQuran')
        )
      }
      {
        presableMenu(
          '#EE6C4D',
          allLogo.imgExam,
          'Exam',
          'Let\'s test your skills',
          () => alert('progress')
        )
      }
      {
        presableMenu(
          '#D4E09B',
          allLogo.imgRanking,
          'Ranking',
          'See your ranking here',
          () => NavigatorService.navigate('Ranking')
        )
      }
    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: toDp(20),
    height: toDp(30),
    fontWeight: '500',
    color: 'white',
  },
  desc: {
    fontSize: toDp(10),
    height: toDp(15),
    fontWeight: '500',
    color: 'white',
  },
  presableMenu: {
    width: '90%',
    height: toDp(88),
    borderRadius: toDp(25),
    shadowColor: "#000",
    shadowOffset: {
    	width: 0,
    	height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
    flexDirection: 'row',
    marginTop: toDp(16)
  },
  imgUnderstand: {
    width: toDp(50),
    height: toDp(57),
    marginLeft: toDp(19),
    marginTop: toDp(12)
  },
  viewText: {
    marginLeft: toDp(18),
    //alignItems: 'center',
    justifyContent: 'center'
  }
});

export default Home;
