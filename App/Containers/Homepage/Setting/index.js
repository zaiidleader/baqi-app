


import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  StatusBar,
  ImageBackground,
  Pressable,
  Alert,
  FlatList
} from "react-native";
import { allLogo } from '@Assets';
import { toDp } from '@percentageToDP';

import NavigatorService from '@NavigatorService'
import Loader from '@Loader'
import Header from '@Header'
import { firestore } from '../../../Configs/firebase'

const Setting = (props) => {

  const [state, setState] = useState({
    loading: false,
    arrayLevel: []
  })

  useEffect(() => {
    getLevel()
  }, [])

  const getLevel = () => {
    setState(state => ({...state, loading: true }))
    firestore.collection('soal').get().then(response => {
      let data = response.docs.map(doc => {
        return {
          id: doc.id,
          value: doc.data()
        }
      })
      console.log('data', data);
      setState(state => ({...state, loading: false, arrayLevel: data }))
    })
  }

  const selectMateri = (value) => {
    NavigatorService.navigate('Materi', {value, level: props.navigation.state.params.title})
  }

  const presableMenu = (item) => {
    return (
      <View style={[styles.presableMenu, {backgroundColor: 'cyan'}]}>
        <View style={styles.viewText}>
          <Text style={styles.title}>{item.soal}</Text>
          {
            item.opsi.map((data, index) => {
              return (
                <Pressable style={{padding: toDp(4)}} onPress={() => {
                    if(data === item.jawaban) {
                      alert('selamat anda benar')
                    } else {
                      alert('jawaban anda salah')
                    }
                  }}>
                  <Text>{data}</Text>
                </Pressable>

              )
            })
          }
        </View>
      </View>
    )
  }

  return (
    <View style={styles.container}>
      <View style={styles.viewCenterAbsolute}>
        <Image source={allLogo.logo} style={styles.logo} />
      </View>
      <Loader loading={state.loading} />
      <View style={styles.content}>
        {
          /*state.arrayLevel.map((data, index) => {
            return (
              presableMenu(data.value.backgroundColor, data.value.name, () => selectMateri(data.value.name))
            )
          })*/
        }
        <View style={{width: '100%'}}>
          <FlatList
            data={state.arrayLevel}
            renderItem={({item, index}) => {
              return (
                presableMenu(item.value)
              )
            }}
            ListFooterComponent={() => <View style={{height: toDp(24)}} />}
          />
        </View>
      </View>



    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  content: {
    width: '100%',
    alignItems: 'center'
  },
  viewCenterAbsolute: {
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
  },
  logo: {
    width: toDp(256),
    height: toDp(270),
    resizeMode: 'contain'
  },
  icMateri: {
    width: toDp(39),
    height: toDp(52),
    resizeMode: 'contain',
    marginLeft: toDp(24),
    marginTop: toDp(6)
  },
  presableMenu: {
    width: '90%',
    marginLeft: toDp(16),
    //height: toDp(66),
    height: 'auto',
    paddingLeft: toDp(16),
    paddingBottom: toDp(16),
    borderRadius: toDp(25),
    shadowColor: "#000",
    shadowOffset: {
    	width: 0,
    	height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
    flexDirection: 'row',
    marginTop: toDp(16)
  },
  viewText: {
    width: '100%',
    justifyContent: 'center',
  },
  title: {
    fontSize: toDp(20),
    height: toDp(30),
    fontWeight: '500',
    color: 'black',
    width: '100%',
    textAlign: 'center',
    marginTop: toDp(4)
  },

});

export default Setting;
