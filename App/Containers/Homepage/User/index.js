import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  StatusBar,
  ImageBackground,
  Pressable,
  AsyncStorage,
  Alert,
  Dimensions,
  TouchableOpacity,
  Linking,
  Platform
} from "react-native";
import { allLogo } from '@Assets';
import { toDp } from '@percentageToDP';

import Modal from 'react-native-modal'
import ImagePicker from 'react-native-image-crop-picker'

import storage from '@react-native-firebase/storage'

import NavigatorService from '@NavigatorService';

const { width, height } = Dimensions.get('window')
const User = (props) => {

  const [state, setState] = useState({
    photo: 'https://git.qlue.id/uploads/-/system/user/avatar/31/avatar.png?width=400',
    name: '',
    phone: '',
    email: '',
    modalVisible: false,
    options: {
      width: 750,
      height: 750,
      cropping: true,
    },
  })

  useEffect(() => {
    AsyncStorage.getItem('users').then(response => {
      console.log('response', response);
      let users = JSON.parse(response)
      setState(state => ({...state,
        name: users?.name,
        phone: users?.phone,
        email: users?.email,
        photo: users?.picture
      }))
    }).catch(err => {
      console.log('err', err)
    })
  }, [])

  const logout = () => {
    Alert.alert(
      "Confirmation",
      "Are you sure Logout ?",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => {
          AsyncStorage.clear()
          NavigatorService.reset('Login')
        }}
      ]
    )
  }

  const camera = () => {
    ImagePicker.openCamera(state.options).then(response => {
      processUpload(response)
    }).catch(err => {
      console.log(err)
      if(err == 'Error: Required permission missing' || err == 'User did not grant camera permission.') {
        Alert.alert(
          'Pengaturan',
          'Akses ambil foto belum diaktifkan.\nBerikan akses untuk memulai mengambil gambar. Aktifkan akses ambil foto dari Pengaturan.',
          [
            {text: 'Nanti Saja', onPress: () => console.log('Cancel')},
            {text: 'Aktifkan', onPress: () => {
              Linking.openSettings();
            }},
          ],
          {cancelable: false},
        );
      }
    })
  }

  const gallery = () => {
    ImagePicker.openPicker(state.options).then(response => {
      processUpload(response)
    }).catch(err => {
      console.log(err)
      if(err == 'Error: Required permission missing' || err == 'Error: Cannot access images. Please allow access if you want to be able to select images.') {
        Alert.alert(
          'Pengaturan',
          'Akses pilih foto belum diaktifkan.\nBerikan akses untuk memulai mengambil gambar. Aktifkan akses pilih foto dari Pengaturan.',
          [
            {text: 'Nanti Saja', onPress: () => console.log('Cancel')},
            {text: 'Aktifkan', onPress: () => {
              Linking.openSettings();
            }},
          ],
          {cancelable: false},
        );
      }
    })
  }

  const randomString = (len, charSet) => {
    charSet = charSet || 'abcdefghijklmnopqrstuvwxyz0123456789';
    var randomString = '';
    for (var i = 0; i < len; i++) {
        var randomPoz = Math.floor(Math.random() * charSet.length);
        randomString += charSet.substring(randomPoz,randomPoz+1);
    }
    return randomString;
}

  const processUpload = (response) => {
    if(response.didCancel) {
    } else {
      console.log('response.path', response.path);

      const reference = storage().ref(randomString(8)+'.jpg')
      reference.putFile(response.path).then(response => {
        console.log('response', response)
        let picture = 'https://firebasestorage.googleapis.com/v0/b/'+response.metadata.bucket+'/o/'+(Platform.OS === 'android' ? response.metadata.fullPath : response.metadata.name)+'?alt=media'
        console.log('picture', picture);
        setState(state => ({...state, modalVisible: false, photo: picture}))
        //alert(JSON.stringify(picture))
      }).catch(error => {
        console.log('error', error)
        setState(state => ({...state, modalVisible: false}))
        //alert(JSON.stringify(error))
      })
    }
  }

  const renderModal = () => {
    return (
      <Modal
        onBackdropPress={() => setState(state => ({...state, modalVisible: false})) }
        isVisible={state.modalVisible}
        style={styles.bottomModal}>

        <View style={styles.viewRootModal}>
          <View style={[styles.modalBox, {backgroundColor: '#FFFFFF', height: toDp(192)}]}>
            <View style={styles.viewModalTitle}>
              <TouchableOpacity style={styles.touchSilang} onPress={() => setState(state => ({...state, modalVisible: false})) }>
                <Image source={allLogo.icSilang} style={styles.icSilang} />
              </TouchableOpacity>
              <Text style={[styles.textTitleModal, {color: '#363636'}]}>{'Change Photo Profile'}</Text>
              <View style={styles.touchSilang} />
            </View>

            <View style={{marginTop: toDp(24), marginLeft: toDp(16)}}>

              <View style={styles.viewButton}>
                <Pressable
                  onPress={() => camera() }
                  style={[styles.presableButton, {backgroundColor: '#CBDFBD'}]}
                >
                  <Text style={styles.text}>Camera</Text>
                </Pressable>
                <Pressable
                  onPress={() => gallery() }
                  style={[styles.presableButton, {backgroundColor: '#EE6C4D'}]}>
                  <Text style={styles.text}>Gallery</Text>
                </Pressable>
              </View>

            </View>

          </View>
        </View>
      </Modal>
    )
  }

  const viewMenu = (backgroundColor, title, value) => {
    return (
      <View style={[styles.presableMenu, {backgroundColor}]}>
        <View style={styles.viewText}>
          <Text style={styles.title}>{title}</Text>
          <Text style={styles.desc}>{value}</Text>
        </View>
      </View>
    )
  }

  return (
    <View style={styles.container}>
      {renderModal()}
      <Pressable onPress={() => setState(state => ({...state, modalVisible: true}))}>
        <Image source={{uri: state.photo}} style={styles.imgProfile} />
      </Pressable>
      {viewMenu('#CBDFBD','Name', state.name)}
      {viewMenu('#EE6C4D','Phone', state.phone)}
      {viewMenu('#D4E09B','Email', state.email)}
      <Pressable style={styles.presableLogout} onPress={() => logout()}>
        <Text style={styles.textLogout}>LOGOUT</Text>
      </Pressable>

    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: toDp(20),
    height: toDp(30),
    fontWeight: '500',
    color: 'white',
  },
  desc: {
    fontSize: toDp(10),
    height: toDp(15),
    fontWeight: '500',
    color: 'white',
  },
  presableMenu: {
    width: '90%',
    height: toDp(60),
    borderRadius: toDp(25),
    shadowColor: "#000",
    shadowOffset: {
    	width: 0,
    	height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
    flexDirection: 'row',
    marginTop: toDp(16)
  },
  imgUnderstand: {
    width: toDp(50),
    height: toDp(57),
    marginLeft: toDp(19),
    marginTop: toDp(12)
  },
  viewText: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center'
  },
  imgProfile: {
    width: toDp(70),
    height: toDp(70),
    borderRadius: toDp(35),
    //position: 'absolute',
    //zIndex: 1
  },
  presableLogout: {
    padding: toDp(8),
    marginTop: toDp(24)
  },
  textLogout: {
    fontSize: toDp(20),
    fontWeight: 'bold',
    color: 'black',
    letterSpacing: toDp(1)
  },
  bottomModal: {
    justifyContent: "flex-end",
    margin: 0,
  },
  viewRootModal: {
    width,
    position: 'absolute',
    bottom: 0
  },
  modalBox: {
    width,
    height: toDp(165),
    backgroundColor: '#111111',
    borderTopLeftRadius: toDp(16),
    borderTopRightRadius: toDp(16)
  },
  modalRow: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  viewModalTitle: {
    marginTop: toDp(24),
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: toDp(16)
  },
  touchSilang: {
    padding: toDp(4),
  },
  icSilang: {
    width: toDp(24),
    height: toDp(24),
  },
  textTitleModal: {
    fontSize: toDp(16),
    color: '#363636',
    fontWeight: 'bold'
  },
  viewButton: {
    marginTop: toDp(16),
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  presableButton: {
    width: 'auto',
    paddingHorizontal: toDp(16),
    height: toDp(39),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: toDp(25),
    shadowColor: "#000",
    shadowOffset: {
    	width: 0,
    	height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  text: {
    fontSize: toDp(16),
    fontWeight: 'bold',
    color: 'white',
  }
});

export default User;
