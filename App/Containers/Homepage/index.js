import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  StatusBar,
  ImageBackground,
  Pressable
} from "react-native";
import { allLogo } from '@Assets';
import { toDp } from '@percentageToDP';

import NavigatorService from '@NavigatorService'

import Home from './Home'
import User from './User'
import Setting from './Setting'

const Homepage = () => {

  const [state, setState] = useState({
    content: 'home', // home, user, setting
  })

  return (
    <View style={styles.container}>
      <StatusBar barStyle="dark-content" translucent={true} backgroundColor={'transparent'} />
      <View style={styles.header}>
        <Image source={allLogo.backgroundHeader} style={styles.backgroundHeader} />
        <Text style={styles.title}>Lets learn Al Quran{'\n'}With BaQi</Text>
      </View>

      <View style={styles.content}>
        {
          state.content == 'home' ?
            <Home />
          : state.content == 'user' ?
            <User />
          :
            <Setting />
        }
      </View>

      <View style={styles.footer}>
        <Pressable style={[styles.presable, {backgroundColor: state.content === 'home' ? '#378561' : '#52B788'}]} onPress={() => setState(state => ({...state, content: 'home' }))}>
          <Image source={allLogo.icHome} style={styles.icon} />
        </Pressable>
        <Pressable style={[styles.presable, {backgroundColor: state.content === 'user' ? '#378561' : '#52B788'}]} onPress={() => setState(state => ({...state, content: 'user' }))}>
          <Image source={allLogo.icUser} style={styles.icon} />
        </Pressable>
        <Pressable style={[styles.presable, {backgroundColor: state.content === 'setting' ? '#378561' : '#52B788'}]} onPress={() => setState(state => ({...state, content: 'setting' }))}>
          <Image source={allLogo.icSetting} style={styles.icon} />
        </Pressable>
      </View>

    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignItems: 'center',
  },
  header: {
    width: '100%',
    height: toDp(184),
    backgroundColor: '#52B788',
    borderBottomLeftRadius: toDp(26),
    borderBottomRightRadius: toDp(26),
    shadowColor: "#000",
    shadowOffset: {
    	width: 0,
    	height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
  },
  content: {
    flex: 1,
    width: '100%'
  },
  footer: {
    width: '100%',
    height: toDp(75),
    backgroundColor: "#52B788",
    flexDirection: 'row',
    borderTopLeftRadius: toDp(26),
    borderTopRightRadius: toDp(26),
  },
  presable: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  icon: {
    width: toDp(31),
    height: toDp(31),
    resizeMode: 'contain'
  },
  title: {
    fontWeight: '800',
    fontSize: toDp(20),
    color: 'white',
    marginTop: toDp(80),
    marginLeft: toDp(16)
  },
  backgroundHeader: {
    width: '100%',
    height: toDp(148),
    resizeMode: 'contain',
    position: 'absolute',
    right: toDp(-48),
    bottom: 0
  }

});

export default Homepage;
