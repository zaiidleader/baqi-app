import React, { useEffect, useState } from 'react'
import {
  StyleSheet,
  Text,
  View,
  Image,
  StatusBar,
  TextInput,
  Pressable,
  Platform,
  AsyncStorage
} from "react-native";
import { allLogo } from '@Assets';
import { toDp } from '@percentageToDP';

import NavigatorService from '@NavigatorService'
import Loader from '@Loader'
import { firestore } from '../../Configs/firebase'


const Login = (props) => {

  const [state, setState] = useState({
    loading: false,
    secureTextEntry: true,
    username: '',
    password: ''
  })

  const login = () => {
    //alert(`username = ${state.username} password = ${state.password}`)
    // show loading
    setState(state => ({...state, loading: true }))
    firestore.collection('users').where('username', '==', state.username).where('password', '==', state.password).get().then(response => {
      let data = response.docs.map(doc => {
        return {
          id: doc.id,
          value: doc.data()
        }
      })
      setState(state => ({...state, loading: false }))
      console.log('data', data);
      if(data.length === 0) {
        alert('Wrong Username or Password')
      } else {
        //save Async Storage
        AsyncStorage.setItem('users', JSON.stringify(data[0].value))
        NavigatorService.reset('Homepage')
      }
    })
  }

  /*const loginGoogle = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      console.log('loginGoogle ', userInfo);
      const {idToken, user} = userInfo;
      const body = {
        provider: 'google',
        uid: user.id,
        email: user.email,
        image: user.photo,
        name: user.name,
      };
      alert(JSON.stringify(userInfo))
      //console.log('body', body);
      //authStore.authSosmed(body);
    } catch (error) {
      alert(JSON.stringify(error))
      console.log('ERROR LoginGoogle ', error, error.message);
    }
  };*/

  return (
    <View style={styles.container}>
      <StatusBar barStyle="dark-content" translucent={true} backgroundColor={'transparent'} />
      <Loader loading={state.loading} />
      <Image source={allLogo.logo} style={styles.logo} />
      <Text style={styles.title}>BAQi</Text>
      <Text style={styles.desc}>Bahasa Quran Interaktif</Text>
      <View style={styles.viewContent}>

        <Text style={styles.titleForm}>Login</Text>
        <Text style={styles.descForm}>Silahkan masuk untuk melanjutkan</Text>

        <View style={{height: toDp(46)}} />

        <Text style={styles.textName}>Username atau Email</Text>
        <TextInput
          autoCapitalize={'none'}
          style={styles.textInput}
          placeholder={'Username'}
          placeholderTextColor={'grey'}
          value={state.username}
          onChangeText={(text) => setState(state => ({...state, username: text })) }
        />
        <View style={{marginTop: toDp(16)}}>
          <Text style={styles.textName}>Password</Text>
          <TextInput
            autoCapitalize={'none'}
            style={[styles.textInput, {marginTop: toDp(8)}]}
            placeholder={'Password'}
            placeholderTextColor={'grey'}
            secureTextEntry={state.secureTextEntry}
            value={state.password}
            onChangeText={(text) => setState(state => ({...state, password: text })) }
          />
          <Pressable style={styles.presableShow} onPress={() => setState(state => ({...state, secureTextEntry: !state.secureTextEntry }))}>
            <Image source={state.secureTextEntry ? allLogo.icVisibilityOff : allLogo.icVisibilityOn} style={styles.icVisibility} />
          </Pressable>
        </View>
        <View style={styles.viewRow}>
          <Pressable style={styles.presableForgot}>
            <Text style={styles.textForgot}>Forgot Password ?</Text>
          </Pressable>
          <Pressable style={styles.presableLogin} onPress={() => login()}>
            <Text style={styles.textLogin}>Login</Text>
          </Pressable>
        </View>

        <View style={[styles.positionRight, {marginTop: toDp(60)}]}>
          <Pressable
            onPress={() => NavigatorService.navigate('Register')}
            style={styles.presableForgot}
          >
            <Text style={styles.textCreate}>Create Account</Text>
          </Pressable>
        </View>

      </View>
      <Text style={styles.textDont}>Or Login With</Text>
      <View style={styles.rowFooter}>
        <Pressable style={styles.presableClick} onPress={() => alert('in progress')}>
          <Image source={allLogo.icGoogle} style={styles.icon} />
        </Pressable>
        <View style={{width: toDp(16)}} />
        <Pressable style={styles.presableClick}>
          <Image source={allLogo.icFacebook} style={styles.icon} />
        </Pressable>
      </View>
    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'white',
    paddingTop: toDp(24)
  },
  logo: {
    width: toDp(155),
    height: toDp(154),
    position: 'absolute',
    zIndex: 1,
    top: 0,
    left: 0
  },
  title: {
    fontSize: toDp(30),
    fontWeight: 'bold',
    color: '#000000',
    textAlign: 'center'
  },
  desc: {
    fontSize: toDp(18),
    color: '#000000',
    textAlign: 'left'
  },
  viewContent: {
    zIndex: 2,
    width: '90%',
    height: 'auto',
    backgroundColor: '#52B788',
    borderRadius: toDp(24),
    marginTop: toDp(16),
    padding: toDp(16),
    shadowColor: "#000",
    shadowOffset: {
    	width: 0,
    	height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
  },
  textInput: {
    width: '100%',
    height: toDp(48),
    backgroundColor: '#F2F3F3',
    paddingHorizontal: toDp(8),
    borderRadius: toDp(4),
    marginTop: toDp(8)
  },
  positionRight: {
    width: '100%',
    alignItems: 'flex-end',
    marginTop: toDp(8)
  },
  textForgot: {
    textAlign: 'left'
  },
  textDont: {
    marginTop: toDp(16),
    fontSize: toDp(12),
    color: '#000000',
  },
  presableClick: {
    padding: toDp(4)
  },
  textClick: {
    fontSize: toDp(14),
    fontWeight: 'bold',
    color: '#009EE2',
  },
  presableShow: {
    padding: toDp(4),
    position: 'absolute',
    right: toDp(8),
    top: Platform.OS === 'ios' ? toDp(30) : toDp(34)
  },
  icVisibility: {
    width: toDp(24),
    height: toDp(24),
    tintColor: 'grey'
  },
  rowFooter: {
    marginTop: toDp(8),
    flexDirection: 'row'
  },
  icon: {
    width: toDp(30),
    height: toDp(30)
  },
  textName: {
    fontSize: toDp(12),
    color: '#000000',
  },
  viewRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: toDp(24)
  },
  presableForgot: {
    width: toDp(130),
    paddingVertical: toDp(4),
  },
  presableLogin: {
    width: toDp(100),
    height: toDp(34),
    backgroundColor: '#2D6A4F',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: toDp(20),
  },
  textLogin: {
    fontSize: toDp(16),
    letterSpacing: toDp(1),
    fontWeight: 'bold',
    color: 'white',
    textAlign: 'center'
  },
  textCreate: {
    textAlign: 'right'
  },
  titleForm: {
    fontSize: toDp(20),
    fontWeight: 'bold',
    color: 'white',
    textAlign: 'center',
    marginTop: toDp(4)
  },
  descForm: {
    fontSize: toDp(12),
    color: '#000000',
    textAlign: 'center',
    marginTop: toDp(14)
  }
});

export default Login;
