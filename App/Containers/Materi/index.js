import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  StatusBar,
  ImageBackground,
  Pressable,
  Alert,
  FlatList
} from "react-native";
import { allLogo } from '@Assets';
import { toDp } from '@percentageToDP';

import Loader from '@Loader'
import Header from '@Header'
import { firestore } from '../../Configs/firebase'
import NavigatorService from '@NavigatorService'

const Materi = (props) => {

  const [state, setState] = useState({
    loading: false,
    arrayMateri: []
  })

  useEffect(() => {
    getMateri()
  }, [])

  const getMateri = () => {
    setState(state => ({...state, loading: true }))
    firestore.collection('materi').where('level', '==', props.navigation.state.params.level).where('sub_levels', '==', props.navigation.state.params.value.name).get().then(response => {
      let data = response.docs.map(doc => {
        return {
          id: doc.id,
          value: doc.data()
        }
      })
      console.log('data', data);
      setState(state => ({...state, loading: false, arrayMateri: data }))
    })
  }

  const selectMateri = (level) => {
    alert(level)
  }

  const generateBackgroundColor = (index) => {
    if(index % 3 == 0) {
      return '#EE6C4D'
    } else if(index % 3 == 1) {
      return '#D4E09B'
    } else if(index % 3 == 2) {
      return '#CBDFBD'
    } else {
      return 'red' // gak masuk sini ...
    }
  }

  const openLinks = (type, link, value) => {
    if(type === 'watch') {
      NavigatorService.navigate('Watch', {link, value})
    } else if(type === 'test'){
      NavigatorService.navigate('Test', {link})
    } else {
      NavigatorService.navigate('Read', {link})
    }
  }

  const presableMenu = (item, index) => {
    return (
      <Pressable style={[styles.presableMenu, {backgroundColor: generateBackgroundColor(index)}]} onPress={() => alert('asdf')}>
        <View style={styles.viewText}>
          <Text style={styles.title}>{item.value.information}</Text>
          <View style={styles.row}>
            <Pressable style={{padding: toDp(4)}} onPress={() => openLinks('watch', item.value.watch, item.value)}>
              <Text style={styles.textMenu}>Watch</Text>
            </Pressable>
            <Pressable style={{padding: toDp(4)}} onPress={() => openLinks('read', item.value.read, '')}>
              <Text style={styles.textMenu}>Read</Text>
            </Pressable>
            <Pressable style={{padding: toDp(4)}} onPress={() => openLinks('test', item.value.test, '')}>
              <Text style={styles.textMenu}>Test</Text>
            </Pressable>
          </View>
        </View>
      </Pressable>
    )
  }

  return (
    <View style={styles.container}>
      <View style={styles.viewCenterAbsolute}>
        <Image source={allLogo.logo} style={styles.logo} />
      </View>
      <Loader loading={state.loading} />
      <Header
        title={props.navigation.state.params.value.name}
        onPress={() => props.navigation.goBack()}
      />
      <View style={styles.content}>
        {
          /*state.arrayMateri.map((data, index) => {
            return (
              presableMenu(data.value.backgroundColor, data.value.name, () => selectMateri(data.value.name))
            )
          })*/
        }
        <View style={{width: '100%'}}>
          <FlatList
            data={state.arrayMateri}
            renderItem={({item, index}) => {
              return (
                presableMenu(item, index)
              )
            }}
            ListFooterComponent={() => <View style={{height: toDp(150)}} />}
          />
        </View>
      </View>
      <View style={styles.footer}>
        <Pressable style={styles.presableMenuFooter} onPress={() => NavigatorService.navigate('Test', {link: props.navigation.state.params.value.ujian_pelajaran})}>
          <Text style={styles.textFooter}>Ujian {props.navigation.state.params.value.name.split(' ')[1]} Pelajaran</Text>
        </Pressable>
        <View style={styles.line} />
        <Pressable style={styles.presableMenuFooter} onPress={() => NavigatorService.navigate('Test', {link: props.navigation.state.params.value.ujian_grammer})}>
          <Text style={styles.textFooter}>Ujian {props.navigation.state.params.value.name.split(' ')[1]} Grammer</Text>
        </Pressable>
      </View>

    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  content: {
    width: '100%',
    alignItems: 'center'
  },
  viewCenterAbsolute: {
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
  },
  logo: {
    width: toDp(256),
    height: toDp(270),
    resizeMode: 'contain'
  },
  icMateri: {
    width: toDp(39),
    height: toDp(52),
    resizeMode: 'contain',
    marginLeft: toDp(24),
    marginTop: toDp(6)
  },
  presableMenu: {
    width: '90%',
    marginLeft: toDp(16),
    height: toDp(66),
    borderRadius: toDp(25),
    shadowColor: "#000",
    shadowOffset: {
    	width: 0,
    	height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
    flexDirection: 'row',
    marginTop: toDp(16)
  },
  viewText: {
    width: '100%',
    justifyContent: 'center',
  },
  title: {
    fontSize: toDp(20),
    height: toDp(30),
    fontWeight: '500',
    color: 'white',
    width: '100%',
    textAlign: 'center',
    marginTop: toDp(4)
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: toDp(36)
  },
  textMenu: {
    fontSize: toDp(15),
    fontWeight: '500',
    color: 'white',
    textAlign: 'center',
  },
  footer: {
    width: '100%',
    height: toDp(60),
    borderTopWidth: toDp(1),
    borderTopColor: 'black',
    backgroundColor: '#F2F3F3',
    position: 'absolute',
    bottom: 0,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  presableMenuFooter: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  textFooter: {
    fontSize: toDp(15),
    fontWeight: '500',
  },
  line: {
    width: toDp(1),
    height: toDp(24),
    backgroundColor: 'black'
  }
});

export default Materi;
