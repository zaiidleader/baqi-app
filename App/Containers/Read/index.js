import React, { useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  StatusBar,
  ImageBackground,
  Pressable,
  Dimensions
} from "react-native";
import { allLogo } from '@Assets';
import { toDp } from '@percentageToDP';
import Header from '@Header'
import Pdf from 'react-native-pdf';

const Read = (props) => {
  return (
    <View style={styles.container}>
      <Header
        title={'Read'}
        onPress={() => props.navigation.goBack()}
      />
      <Pdf
        source={{uri: props.navigation.state.params.link}}
        style={styles.pdf}
      />
    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  title: {
    fontSize: toDp(30),
    fontWeight: 'bold',
    color: 'black',
  },
  pdf: {
    flex:1,
    width:Dimensions.get('window').width,
    height:Dimensions.get('window').height,
  },
});

export default Read;
