import React, { useEffect, useState } from 'react'
import {
  StyleSheet,
  Text,
  View,
  Image,
  StatusBar,
  TextInput,
  Pressable,
  Platform
} from "react-native";
import { allLogo } from '@Assets';
import { toDp } from '@percentageToDP';

import Loader from '@Loader'
import { firestore } from '../../Configs/firebase'

const Register = (props) => {

  const [state, setState] = useState({
    loading: false,
    secureTextEntry: true,
    name: '',
    email: '',
    phone: '',
    username: '',
    password: ''
  })

  const signUp = () => {
    let body = {
      name: state.name,
      email: state.email,
      phone: state.phone,
      username: state.username,
      password: state.password
    }

    setState(state => ({...state, loading: true }))
    firestore.collection('users').add(body).then((docRef) => {
      console.log('docRef', docRef);
      setState(state => ({...state, loading: false }))
    }).catch((error) => {
      console.log("Error adding document: ", error);
      setState(state => ({...state, loading: false }))
    })

  }

  return (
    <View style={styles.container}>
      <StatusBar barStyle="light-content" translucent={true} backgroundColor={'transparent'} />
      <Loader loading={state.loading} />
      <Image source={allLogo.logo} style={styles.logo} />
      <View style={styles.viewContent}>

        <Text style={styles.titleForm}>Sign Up</Text>

        <View style={{height: toDp(16)}} />

        <Text style={[styles.textName, {marginTop: toDp(16)}]}>Name</Text>
        <TextInput
          autoCapitalize={'none'}
          style={styles.textInput}
          placeholder={'Name'}
          placeholderTextColor={'grey'}
          value={state.name}
          onChangeText={(name) => setState(state => ({...state, name }))}
        />

        <Text style={[styles.textName, {marginTop: toDp(16)}]}>Email</Text>
        <TextInput
          autoCapitalize={'none'}
          style={styles.textInput}
          placeholder={'Email'}
          placeholderTextColor={'grey'}
          value={state.email}
          onChangeText={(email) => setState(state => ({...state, email }))}
        />

        <Text style={[styles.textName, {marginTop: toDp(16)}]}>Phone</Text>
        <TextInput
          autoCapitalize={'none'}
          style={styles.textInput}
          placeholder={'Phone number'}
          placeholderTextColor={'grey'}
          value={state.phone}
          onChangeText={(phone) => setState(state => ({...state, phone }))}
        />

        <Text style={[styles.textName, {marginTop: toDp(16)}]}>Username</Text>
        <TextInput
          autoCapitalize={'none'}
          style={styles.textInput}
          placeholder={'Username'}
          placeholderTextColor={'grey'}
          value={state.username}
          onChangeText={(username) => setState(state => ({...state, username }))}
        />

        <View style={{marginTop: toDp(16)}}>
          <Text style={styles.textName}>Password</Text>
          <TextInput
            autoCapitalize={'none'}
            style={[styles.textInput, {marginTop: toDp(8)}]}
            placeholder={'Password'}
            placeholderTextColor={'grey'}
            secureTextEntry={state.secureTextEntry}
            onChangeText={(password) => setState(state => ({...state, password }))}
          />
          <Pressable style={styles.presableShow} onPress={() => setState(state => ({...state, secureTextEntry: !state.secureTextEntry })) }>
            <Image source={state.secureTextEntry ? allLogo.icVisibilityOff : allLogo.icVisibilityOn} style={styles.icVisibility} />
          </Pressable>
        </View>
        <View style={styles.viewRow}>
          <Pressable
            onPress={() => props.navigation.goBack() }
            style={styles.presableForgot}
          >
            <Text style={styles.textForgot}>Login</Text>
          </Pressable>
          <Pressable style={styles.presableLogin} onPress={() => signUp()}>
            <Text style={styles.textLogin}>Sign Up</Text>
          </Pressable>
        </View>
      </View>
      <Text style={styles.textDont}>Or Login With</Text>
      <View style={styles.rowFooter}>
        <Pressable style={styles.presableClick}>
          <Image source={allLogo.icGoogle} style={styles.icon} />
        </Pressable>
        <View style={{width: toDp(16)}} />
        <Pressable style={styles.presableClick}>
          <Image source={allLogo.icFacebook} style={styles.icon} />
        </Pressable>
      </View>
    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'white',
  },
  logo: {
    width: toDp(155),
    height: toDp(154),
    position: 'absolute',
    zIndex: 1,
    bottom: 0,
    left: 0
  },
  title: {
    fontSize: toDp(30),
    fontWeight: 'bold',
    color: '#000000',
    textAlign: 'center'
  },
  desc: {
    fontSize: toDp(18),
    color: '#000000',
    textAlign: 'left'
  },
  viewContent: {
    zIndex: 2,
    width: '100%',
    height: 'auto',
    backgroundColor: '#52B788',
    borderBottomLeftRadius: toDp(24),
    borderBottomRightRadius: toDp(24),
    padding: toDp(16),
    shadowColor: "#000",
    shadowOffset: {
    	width: 0,
    	height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
  },
  textInput: {
    width: '100%',
    height: toDp(48),
    backgroundColor: '#F2F3F3',
    paddingHorizontal: toDp(8),
    borderRadius: toDp(4),
    marginTop: toDp(8)
  },
  positionRight: {
    width: '100%',
    alignItems: 'flex-end',
    marginTop: toDp(8)
  },
  textForgot: {
    textAlign: 'left'
  },
  textDont: {
    marginTop: toDp(16),
    fontSize: toDp(12),
    color: '#000000',
  },
  presableClick: {
    padding: toDp(4)
  },
  textClick: {
    fontSize: toDp(14),
    fontWeight: 'bold',
    color: '#009EE2',
  },
  presableShow: {
    padding: toDp(4),
    position: 'absolute',
    right: toDp(8),
    top: Platform.OS === 'ios' ? toDp(30) : toDp(34)
  },
  icVisibility: {
    width: toDp(24),
    height: toDp(24),
    tintColor: 'grey'
  },
  rowFooter: {
    marginTop: toDp(8),
    flexDirection: 'row'
  },
  icon: {
    width: toDp(30),
    height: toDp(30)
  },
  textName: {
    fontSize: toDp(12),
    color: '#000000',
  },
  viewRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: toDp(24)
  },
  presableForgot: {
    width: toDp(130),
    paddingVertical: toDp(4),
  },
  presableLogin: {
    width: toDp(100),
    height: toDp(34),
    backgroundColor: '#2D6A4F',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: toDp(20),
  },
  textLogin: {
    fontSize: toDp(16),
    letterSpacing: toDp(1),
    fontWeight: 'bold',
    color: 'white',
    textAlign: 'center'
  },
  textCreate: {
    textAlign: 'right'
  },
  titleForm: {
    fontSize: toDp(20),
    fontWeight: 'bold',
    color: 'white',
    textAlign: 'center',
    marginTop: toDp(4)
  },
  descForm: {
    fontSize: toDp(12),
    color: '#000000',
    textAlign: 'center',
    marginTop: toDp(14)
  }
});

export default Register;
