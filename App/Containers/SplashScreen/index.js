import React, { useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  StatusBar,
  ImageBackground,
  Pressable,
  AsyncStorage
} from "react-native";
import { allLogo } from '@Assets';
import { toDp } from '@percentageToDP';

import NavigatorService from '@NavigatorService'

const SplashScreen = () => {

  useEffect(() => {
    setTimeout(function () {
      AsyncStorage.getItem('users').then(response => {
        console.log('response', response);
        if(response == null) {
          NavigatorService.reset('Login')
        } else {
          NavigatorService.reset('Homepage')
        }
      }).catch(err => {
        console.log('err', err)
      })
    }, 3000);
  }, [])

  return (
    <View style={styles.container}>
      <StatusBar barStyle="dark-content" translucent={true} backgroundColor={'transparent'} />
      <View style={styles.content}>
        <ImageBackground source={allLogo.background} resizeMode="cover" style={styles.background}>
          <Text style={styles.title}>BAQI</Text>
          <Text style={styles.desc}>{'Bahasa Quran Interaktif'}</Text>
          <Pressable
            //onPress={() => NavigatorService.reset('Login')}
            style={styles.buttonGet}
          >
            <Text style={styles.textGet}>Get Ready</Text>
          </Pressable>
        </ImageBackground>
      </View>
    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
  content: {
    width: toDp(296),
    height: toDp(437),
    backgroundColor: '#52B788',
    borderRadius: toDp(25)
  },
  background: {
    width: toDp(296),
    height: toDp(437),
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: toDp(30),
    fontWeight: 'bold',
    color: 'white',
  },
  desc: {
    fontSize: toDp(18),
    fontWeight: 'bold',
    color: 'white',
  },
  buttonGet: {
    width: toDp(165),
    height: toDp(46),
    backgroundColor: '#B7E4C7',
    borderRadius: toDp(25),
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: toDp(-23)
  },
  textGet: {
    fontSize: toDp(20),
    fontWeight: 'bold',
    color: 'black',
  }
});

export default SplashScreen;
