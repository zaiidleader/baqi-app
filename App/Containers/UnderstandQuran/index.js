import React, { useEffect, useState } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  StatusBar,
  ImageBackground,
  Pressable,
  Alert,
  FlatList
} from "react-native";
import { allLogo } from '@Assets';
import { toDp } from '@percentageToDP';

import NavigatorService from '@NavigatorService'
import Loader from '@Loader'
import Header from '@Header'
import { firestore } from '../../Configs/firebase'

const UnderstandQuran = (props) => {

  const [state, setState] = useState({
    loading: false,
    arrayLevel: []
  })

  useEffect(() => {
    getLevel()
  }, [])

  const getLevel = () => {
    setState(state => ({...state, loading: true }))
    firestore.collection('levels').get().then(response => {
      let data = response.docs.map(doc => {
        return {
          id: doc.id,
          value: doc.data()
        }
      })
      console.log('data', data);
      setState(state => ({...state, loading: false, arrayLevel: data }))
    })
  }

  const selectMateri = (level) => {
    if(level === 'Level 2') {
      NavigatorService.navigate('SubLevel', {title: level})
    } else {
      alert('Document is not available yet')
    }
  }

  const presableMenu = (backgroundColor, title, onPress) => {
    return (
      <Pressable style={[styles.presableMenu, {backgroundColor}]} onPress={() => onPress()}>
        <Image source={allLogo.icMateri} style={styles.icMateri} />
        <View style={styles.viewText}>
          <Text style={styles.title}>{title}</Text>
        </View>
      </Pressable>
    )
  }

  return (
    <View style={styles.container}>
      <View style={styles.viewCenterAbsolute}>
        <Image source={allLogo.logo} style={styles.logo} />
      </View>
      <Loader loading={state.loading} />
      <Header
        title={'Understand Qur’an '}
        onPress={() => props.navigation.goBack()}
      />
      <View style={styles.content}>
        {
          /*state.arrayLevel.map((data, index) => {
            return (
              presableMenu(data.value.backgroundColor, data.value.name, () => selectMateri(data.value.name))
            )
          })*/
        }
        <View style={{width: '100%'}}>
          <FlatList
            data={state.arrayLevel}
            renderItem={({item, index}) => {
              return (
                presableMenu(item.value.backgroundColor, item.value.name, () => selectMateri(item.value.name))
              )
            }}
            ListFooterComponent={() => <View style={{height: toDp(24)}} />}
          />
        </View>
      </View>

    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
  },
  content: {
    width: '100%',
    alignItems: 'center'
  },
  viewCenterAbsolute: {
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
  },
  logo: {
    width: toDp(256),
    height: toDp(270),
    resizeMode: 'contain'
  },
  icMateri: {
    width: toDp(39),
    height: toDp(52),
    resizeMode: 'contain',
    marginLeft: toDp(24),
    marginTop: toDp(6)
  },
  presableMenu: {
    width: '90%',
    marginLeft: toDp(16),
    height: toDp(66),
    borderRadius: toDp(25),
    shadowColor: "#000",
    shadowOffset: {
    	width: 0,
    	height: 5,
    },
    shadowOpacity: 0.34,
    shadowRadius: 6.27,
    elevation: 10,
    flexDirection: 'row',
    marginTop: toDp(16)
  },
  viewText: {
    width: '60%',
    marginLeft: toDp(18),
    justifyContent: 'center',
  },
  title: {
    fontSize: toDp(20),
    height: toDp(30),
    fontWeight: '500',
    color: 'white',
    width: '100%',
    textAlign: 'center',
    marginTop: toDp(4)
  },
});

export default UnderstandQuran;
