import React, { useEffect } from "react";
import {
  StyleSheet,
  Text,
  View,
  Image,
  StatusBar,
  ImageBackground,
  Pressable,
  ScrollView
} from "react-native";
import { allLogo } from '@Assets';
import { toDp } from '@percentageToDP';
import Header from '@Header'
import YoutubePlayer from "react-native-youtube-iframe";
import NavigatorService from '@NavigatorService'

const Watch = (props) => {
  return (
    <View style={styles.container}>
      <Header
        title={'Understand Qur’an '}
        onPress={() => props.navigation.goBack()}
      />
      <YoutubePlayer
        width={'100%'}
        height={toDp(300)}
        play={true}
        videoId={props.navigation.state.params.link.split("?v=")[1]}
      />
      <ScrollView style={styles.scrollView}>
        <Text style={styles.desc}>
          {props.navigation.state.params.value.description}
        </Text>
        <View style={styles.viewButton}>
          <Pressable
            onPress={() => NavigatorService.navigate('Read', {link: props.navigation.state.params.value.read})}
            style={[styles.presableButton, {backgroundColor: '#CBDFBD'}]}
          >
            <Text style={styles.text}>Read</Text>
          </Pressable>
          <Pressable
            onPress={() => NavigatorService.navigate('Test', {link: props.navigation.state.params.value.test})}
            style={[styles.presableButton, {backgroundColor: '#EE6C4D'}]}>
            <Text style={styles.text}>Test</Text>
          </Pressable>
        </View>
        <View style={{height: toDp(60)}} />
      </ScrollView>
    </View>
  )
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  title: {
    fontSize: toDp(30),
    fontWeight: 'bold',
    color: 'black',
  },
  scrollView: {
    marginTop: toDp(-80)
  },
  desc: {
    fontSize: toDp(14),
    color: 'black',
    marginHorizontal: toDp(16),
  },
  viewButton: {
    marginTop: toDp(16),
    flexDirection: 'row',
    justifyContent: 'space-around'
  },
  presableButton: {
    width: toDp(100),
    height: toDp(39),
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: toDp(25),
    shadowColor: "#000",
    shadowOffset: {
    	width: 0,
    	height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 5,
  },
  text: {
    fontSize: toDp(20),
    fontWeight: 'bold',
    color: 'white',
  }
});

export default Watch;
