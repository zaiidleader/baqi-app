import { createAppContainer } from "react-navigation";
import { createStackNavigator } from 'react-navigation-stack';

import SplashScreen from '../Containers/SplashScreen'
import Login from '../Containers/Login'
import Register from '../Containers/Register'
import Homepage from '../Containers/Homepage'
import UnderstandQuran from '../Containers/UnderstandQuran'
import SubLevel from '../Containers/SubLevel'
import Materi from '../Containers/Materi'
import Watch from '../Containers/Watch'
import Test from '../Containers/Test'
import Read from '../Containers/Read'
import Ranking from '../Containers/Ranking'


const AppNavigator = createStackNavigator(
  {
    SplashScreen: { screen: SplashScreen },
    Login: { screen: Login },
    Register: { screen: Register },
    Homepage: { screen: Homepage },
    UnderstandQuran: { screen: UnderstandQuran },
    SubLevel: { screen: SubLevel },
    Materi: { screen: Materi },
    Watch: { screen: Watch },
    Test: { screen: Test },
    Read: { screen: Read },
    Ranking: { screen: Ranking },

  },
  {
    headerMode: 'none',
    initialRouteName: 'SplashScreen',
  }
);

export default createAppContainer(AppNavigator)
